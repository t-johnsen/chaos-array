﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chaos_array
{
    /*
     * Chaos-array class. Insert and retrieve items from random places in
     * array. Custom exceptions are thrown when item is inserted into unavailable
     * place, and when retrieving an item that is not there. Exceptions are catched.
     */
    class ChaosArray<T>
    {

        private T[] array = new T[10];

        public ChaosArray()
        {
        }

        /*
         * Method for inserting item using FindSpot() to find available spot.
         */
        public void InsertRandom(T item)
        {
            int spot = FindSpot();
            array[spot] = item;

        }

        /*
         * Retrieving random item from array.
         */
        public T GetRandom()
        {
            int counter = 0;
            int spot;
            bool found = false;

            do
            {
                spot = Shuffle(array.Length);
                try
                {
                    if (array[spot] == null || array[spot].Equals(default(T)))
                    {
                        throw new NoItemFoundException("No item found on index " + spot);
                    }
                    else
                    {
                        return array[spot];
                    }
                }
                catch (NoItemFoundException e)
                {

                    Console.WriteLine(e.Message);
                }

                if(counter > array.Length)
                {
                    Console.WriteLine("This list is so random and full of chaos it stops searching if it cant find anything for a while... (And I'm too lazy to handle an empty list.)");
                    return default;
                }

                counter++;

            } while (found == false);

            return default;
        }

        /*
         * Finding available spot in array and returning it.
         */
        private int FindSpot()
        {
            int counter = 0;
            int spot;
            bool found = false;
            do
            {
                spot = Shuffle(array.Length);

                try
                {
                    if (!(array[spot] == null || array[spot].Equals(default(T))))
                    {
                        throw new SpotNotAvailableException("Spot not available on index " + spot);
                    }
                    else
                    {
                        return spot;
                    }
                }
                catch (SpotNotAvailableException e)
                {

                    Console.WriteLine(e.Message);
                    if ((counter == array.Length))
                    {
                        Array.Resize(ref array, array.Length + 10);
                        return Shuffle(10);
                    }
                }

                counter++;

            } while (found == false);

            return -1;
        }

        /*
         * Returning random number in array.
         */
        private int Shuffle(int min)
        {
            Random random = new Random();
            int randNum = random.Next(array.Length - min, array.Length - 1);
            return randNum;
        }        

        /*
         * Printing all the filled spots in array.
         */
        public void PrintArray()
        {
            int index = 0;
            foreach (T item in array)
            {
                if (!(item == null || item.Equals(default(T)))) Console.WriteLine($"Index {index} contains: '{item}'");
                index++;
            }
        }

    }
}
