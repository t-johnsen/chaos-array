﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chaos_array
{
    /*
     * Exception for when item is not there in array when trying to retrieve.
     */
    [Serializable]
    class NoItemFoundException : Exception
    {

        public NoItemFoundException() { }

        public NoItemFoundException(string message) : base(message) { }

        public NoItemFoundException(string message, Exception innerException) : base(message, innerException) { }
    }
}
