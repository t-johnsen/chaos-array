﻿using System;

namespace chaos_array
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, there! Welcome to chaos array!");

            ChaosArray<int> chaosArrayInts = new ChaosArray<int>() { };
            ChaosArray<string> chaosArrayStrings = new ChaosArray<string>() { };


            chaosArrayStrings.InsertRandom("1st string");
            chaosArrayStrings.InsertRandom("2nd string");
            chaosArrayStrings.InsertRandom("3rd string");
            chaosArrayStrings.InsertRandom("4th string");
            chaosArrayStrings.InsertRandom("5th string");
            chaosArrayStrings.InsertRandom("6th string");
            chaosArrayStrings.InsertRandom("7th string");
            chaosArrayStrings.InsertRandom("8th string");
            chaosArrayStrings.InsertRandom("9th string");
            chaosArrayStrings.InsertRandom("10th string");
            chaosArrayStrings.InsertRandom("TROLL");

            chaosArrayStrings.PrintArray();
            Console.WriteLine("");
            Console.WriteLine("\nGetting random item from list: " + chaosArrayStrings.GetRandom() + "\n");

            chaosArrayInts.InsertRandom(1);
            chaosArrayInts.InsertRandom(2);
            chaosArrayInts.InsertRandom(3);
            chaosArrayInts.InsertRandom(4);
            chaosArrayInts.InsertRandom(5);
            chaosArrayInts.InsertRandom(6);
            chaosArrayInts.InsertRandom(7);
            chaosArrayInts.InsertRandom(8);
            chaosArrayInts.InsertRandom(9);
            chaosArrayInts.InsertRandom(10);
            chaosArrayInts.InsertRandom(99);

            chaosArrayInts.PrintArray();
            Console.WriteLine("");
            Console.WriteLine("\nGetting random item from list: " + chaosArrayInts.GetRandom());
            

        }
    }
}
