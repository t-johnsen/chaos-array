﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chaos_array
{
    /*
     * Exception for when spot is not available in array.
     */
    [Serializable]
    class SpotNotAvailableException : Exception
    {
        public SpotNotAvailableException() { }

        public SpotNotAvailableException(string message) : base(message) { }

        public SpotNotAvailableException(string message, Exception innerException) : base(message, innerException) { }
    }
}
